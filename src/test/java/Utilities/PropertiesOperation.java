package Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class PropertiesOperation {

	static Properties prop = new Properties();

	public static String getPropertyValue(String key) {
		String propPath = ".//config.properties";
		try {
			File f = new File(propPath);
			FileInputStream fis = new FileInputStream(f);
			prop.load(fis);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return prop.get(key).toString();
	}

}
