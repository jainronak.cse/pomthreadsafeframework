package PageClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Utilities.PropertiesOperation;
import testBase.BaseClass;
import testBase.DriverFactory;



public class LoginPage extends BaseClass {

//	@FindBy(id="uid")
//	WebElement uname;

	By uname = By.id("uid");
	By pass = By.id("passw");
	By sub = By.name("btnSubmit");
	By signOff = By.xpath("//font[text()='Sign Off']");

	public LoginPage() {
//		PageFactory.initElements(driver, this);
	}

	public void enterUserName() {
		String username= PropertiesOperation.getPropertyValue("username");
	 DriverFactory.getInstance().getDriver().findElement(uname).sendKeys(username);
	}

	public void enterPassword() {
		String password= PropertiesOperation.getPropertyValue("password");
		 DriverFactory.getInstance().getDriver().findElement(pass).sendKeys(password);
	}

	public void clickSubmit() {
		 DriverFactory.getInstance().getDriver().findElement(signOff).click();
	}

	

	public void login() {
		enterUserName();
		enterPassword();
		clickSubmit();
	}

}
