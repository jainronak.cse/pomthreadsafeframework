package testBase;

import org.openqa.selenium.WebDriver;

public class DriverFactory {

	// singleton pattern
	private static DriverFactory instance=null;
	private DriverFactory() {
		
	}
	public static DriverFactory getInstance() {
		if(instance==null) {
			instance = new DriverFactory();
		}
		return instance;
	}
	
	ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>();
	
	// factory design pattern
	public void setDriver(WebDriver driverParam) {
		driver.set(driverParam);
	}
	
	public WebDriver getDriver() {
		return driver.get();
	}
	public void closeBrowser() {
		driver.get().quit();
		driver.remove();
	}
	
	
}
