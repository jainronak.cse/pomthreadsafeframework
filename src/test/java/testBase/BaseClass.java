package testBase;

import org.openqa.selenium.WebDriver;

import com.fasterxml.jackson.databind.annotation.JsonAppend.Prop;

import Utilities.PropertiesOperation;

public class BaseClass {
	BrowserFactory bf;

	public BaseClass() {
		bf = new BrowserFactory();
	}
	
	public void init() {
		String browser = PropertiesOperation.getPropertyValue("browser");
		String url= PropertiesOperation.getPropertyValue("url");
		WebDriver driverParam= bf.createBrowser(browser);
		DriverFactory.getInstance().setDriver(driverParam);
		DriverFactory.getInstance().getDriver().manage().window().maximize();
		DriverFactory.getInstance().getDriver().get(url);
	}
	
	public void tearDown() {
		DriverFactory.getInstance().closeBrowser();
	}

}
